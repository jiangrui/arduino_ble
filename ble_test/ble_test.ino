#include <SoftwareSerial.h>
#include <string.h>
#include "BLE/BLE.cpp"

SoftwareSerial btSerial(2, 3);  //2:RX, 3:TX

BLE bt = BLE(0);  // 0: Normal Mode, 1: debug mode
String str;

int looop;



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  btSerial.begin(9600);
  bt.begin(&btSerial, &Serial);
  bt.start();
  Serial.println("Arduino Started!");
  delay(2000);

  if(bt.check()==1){
    Serial.println("Reset BLE module : "+bt.reset());
    Serial.println(bt.getName()+" is ready!");
    delay(2000);

    Serial.println("------Setup Finished------");
  }else{
    Serial.println("BLE device start fail!");
  }

  looop = 1;
}

void loop() {
  
  
  scan();

}

void test(){
  Serial.println("--------------------------New Loop : "+ String(looop) +"    Started!--------------------------");
  bt.scanDevice();
  looop++;
}


void scan(){
  bt.scanDevice();
  Serial.print("{\"total\":\"" + String(iBeanum) + "\",\"iBeacons\":[" );
  for(int i=0; i<iBeanum; i++){

    Serial.print("{");
    Serial.print("\"FactoryID\":\"" + btdvs[i].FactoryID + "\",");
    Serial.print("\"iBeaconID\":\"" + btdvs[i].iBeaconID + "\",");
    Serial.print("\"Major\":\"" + btdvs[i].Major + "\",");
    Serial.print("\"Minor\":\"" + btdvs[i].Minor + "\",");
    Serial.print("\"Power\":\"" + btdvs[i].Power + "\",");
    Serial.print("\"Mac\":\"" + btdvs[i].Mac + "\",");
    Serial.print("\"RSSI\":\"" + btdvs[i].RSSI + "\"");
    Serial.print("}");
    if(i!=iBeanum-1){Serial.print(",");}
  }
  Serial.println("]}");

}


