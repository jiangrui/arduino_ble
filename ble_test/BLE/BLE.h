#ifndef BLE_H
#define BLE_H

#include <Arduino.h>
#include <Stream.h>


class BLE
{

  private:
    Stream *serial;
    Stream *btSerial;
    int mode;
    String result;
    String btDevices;
    unsigned long start_time, run_time;
    void debuger(int p, String str);



  public:
    BLE(int mode);
    int Read();
    int Write(String str);
    void Flush();
    void clearBuf();
    int Available();
    String cmd(String q, int wait);
    String reset();
    void start();
    void begin(Stream *btserial, Stream *serial);
    bool check(); //initial check , wait and confirm ble device is OK.
    String getName(); // Get ble device name;
    void scanDevice();  // Scan Bluetooth devices information around
    String setName(String str);
    String iBeaconCheck();

    String setMode();
    String getMode();



};
#endif
