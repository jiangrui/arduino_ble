#include "BLE.h";

// construct a BLE module instance
BLE::BLE(int mode){
  this->mode = mode;
}

// initial setup with Serials (btSerial is for BLE, serial is for debug)
void BLE::begin(Stream *btserial, Stream *serial){
  this->btSerial = btserial;
  this->serial = serial;
}

// Read from buffer
int BLE::Read() {
 return this->btSerial->read();
}


// send to BLE
int BLE::Write(String str){
 this->btSerial->println(str);
 Flush();
}

// empty buffer area
void BLE::Flush(){
 this->btSerial->flush();
}

// clear buffer
void BLE::clearBuf() {
 while(Available() > 0)
   Read();
}

// check if BLE device serial is available.
int BLE::Available(){
  return this->btSerial->available();
}


// Common Query for sending AT+* command
String BLE::cmd(String q, int wait){
  clearBuf();
  String result;
  this->btSerial->print(q);
  delay(wait);
  while (this->btSerial->available()) {
    result += this->btSerial->readString();
  }
  return result;
}


// BLE module reset
String BLE::reset(){
  this->btSerial->print("AT+RESET");
  String res;
  delay(200);
  while(this->btSerial->available()){
    res += this->btSerial->readString();
  }
  return res;
}

void BLE::start(){
  this->cmd("AT+START", 200);
}



// Check if BLE is Ready!
bool BLE::check(){
  clearBuf();
  start_time = millis();
  run_time = 0;
  debuger(0, "Checking BLE module ... ");
  String result;

  this->btSerial->write("AT");
  delay(200);

  while(run_time<2000){
    while(this->btSerial->available()){
      result += this->btSerial->readString();
    }
    run_time = millis() - start_time;
  }
  debuger(3, result);
  if(result == "OK"){
    return 1;
  }else{
    return 0;
  }
}



// 获取硬件名称 get BLE name :
String BLE::getName(){
  clearBuf();
  debuger(0, "Getting Name ... ");
  this->btSerial->write("AT+NAME?");
  delay(200);
  while(this->btSerial->available()){
    result += this->btSerial->readString().substring(8);
  }
  debuger(3, result);
  return result;
}





// 设置BLE硬件名称 set BLE name
String BLE::setName(String str){

  String res;
  clearBuf();

  res = "AT+NAME"+str;
  debuger(0, "Now set name : " + str + " ... ");

  unsigned long start_time, run_time;

  start_time = millis();
  run_time = 0;

  this->btSerial->print(res);
  res = "";
  while(run_time<3000){
    while(this->btSerial->available()){
      res +=this->btSerial->readString();
    }
    run_time = millis() - start_time;
  }

  debuger(3, res);

  return res;
}




// BLE WORK MODE  :  AT+MODE? 0:Transparent mode  1:PIO+Remote Control +Transparent  2:Transparent+Remote Control  default: 0
String BLE::getMode(){
  clearBuf();
  this->btSerial->print("AT+MODE?");
  while(this->btSerial->available()){

  }
}

String BLE::setMode(){

}





// 检查iBeacon开关 : not finished : need parsing result to STRUCT
String BLE::iBeaconCheck(){
  // this->serial->print("Checking iBeacon on/off ...");
  debuger(0, "Checking iBeacon on/off ...");
  int res;
  clearBuf();
  Write("AT+IBEA?");
  delay(300);
  while(this->btSerial->available()){
    res = this->btSerial->read();
  }
  this->serial->println(res);
  debuger(1, String(res));
  if(res==0){
    return "OFF";
  }else if(res==1){
    return "ON";
  }else{
    return "ERROR:"+String(res);
  }
}


struct DVS{
  String FactoryID;
  String iBeaconID;
  String Major;
  String Minor;
  String Power;
  String Mac;
  String RSSI;
};

DVS btdvs[5];
int iBeanum;

// 扫描蓝牙信号:AT+DISI? : not finished , need parsing result to STRUCT
void BLE::scanDevice(){

  start_time = millis();
  run_time = 0;

  clearBuf();
  this->btSerial->write("AT+DISI?");
  debuger(1, "Scanning Device...");

  while( run_time < 1000){
    while( this->btSerial->available() ){
         btDevices += this->btSerial->readString();
    }
    run_time = millis()-start_time;
  }
  //  btDevices = btDevices.substring(16,250);
   btDevices.replace("OK+DISIS", "");
   btDevices.replace("OK+DISCE", "");
  //  btDevices.replace("OK+DISC:", "\n\r");
   btDevices.trim();


   debuger(1, "Total returned data length = "+String(btDevices.length()));
   debuger(1, "Returned data : "+ btDevices);
   int total = btDevices.length();
   iBeanum = total/78;
   debuger(1, "Total Devices Discovered = "+String(iBeanum));

   for(int i=0; i<iBeanum; i++){
     btdvs[i].FactoryID = btDevices.substring(8, 16);
     btdvs[i].iBeaconID = btDevices.substring(17, 49);
     btdvs[i].Major = btDevices.substring(50, 54);
     btdvs[i].Minor = btDevices.substring(54, 58);
     btdvs[i].Power = btDevices.substring(58, 60);
     btdvs[i].Mac = btDevices.substring(61, 73);
     btdvs[i].RSSI = btDevices.substring(74, 78);
     btDevices.remove(0, 78);
     debuger(1, "------ iBeacon " +String(i+1)+ " ------");
     debuger(1, "FactoryID : "+btdvs[i].FactoryID);
     debuger(1, "iBeaconID : "+btdvs[i].iBeaconID);
     debuger(1, "Major : "+btdvs[i].Major);
     debuger(1, "Minor : "+btdvs[i].Minor);
     debuger(1, "Power :"+btdvs[i].Power);
     debuger(1, "Mac : "+btdvs[i].Mac);
     debuger(1, "RSSI : " + btdvs[i].RSSI);
   }


}





// debug print out information, p = 0 : print , p=1: println;
void BLE::debuger(int p, String str){
  if(this->mode != 1 ){return ;}
  if(p == 0){this->serial->print("DEBUG -> "+str);}
  if(p == 1){this->serial->println("DEBUG -> "+str);}
  if(p == 2){this->serial->print(str);}
  if(p == 3){this->serial->println(str);}
}
